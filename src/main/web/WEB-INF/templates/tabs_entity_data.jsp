<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${user.role.name != 'moderator'}">
    <div class="tab-pane fade show active" id="pills-books" role="tabpanel"
         aria-labelledby="pills-home-tab">
        <a class="btn btn-outline-primary" role="button"
           href="${pageContext.request.contextPath}/books/add">add book</a>
        <a class="btn btn-outline-primary" role="button"
           href="${pageContext.request.contextPath}/books/upload">add books from file</a>
        <div class="active-cyan-4 mb-4 search-field">
            <input id="search-books" class="form-control " type="text" placeholder="Search"
                   aria-label="Search" onkeyup="search(id, 'table-books')">
        </div>
        <jsp:include page="books/table_books.jsp"/>
    </div>
    <div class="tab-pane fade" id="pills-authors" role="tabpanel" aria-labelledby="pills-authors-tab">
        <a class="btn btn-outline-primary" role="button" href="authors/add">add author</a>
        <div class="active-cyan-4 mb-4 search-field">
            <input id="search-authors" class="form-control " type="text" placeholder="Search"
                   aria-label="Search" onkeyup="search(id, 'table-authors')">
        </div>
        <jsp:include page="authors/table_authors.jsp"/>
    </div>
    <div class="tab-pane fade" id="pills-bookmarks" role="tabpanel"
         aria-labelledby="pills-bookmarks-tab">
        <a class="btn btn-outline-primary" role="button" href="bookmarks/add">add bookmark</a>
        <div class="active-cyan-4 mb-4 search-field">
            <input id="search-bookmarks" class="form-control " type="text" placeholder="Search"
                   aria-label="Search" onkeyup="search(id, 'table-bookmarks')">
        </div>
        <jsp:include page="bookmarks/table_bookmarks.jsp"/>
    </div>
</c:if>