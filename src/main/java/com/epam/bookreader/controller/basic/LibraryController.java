package com.epam.bookreader.controller.basic;

import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.dao.BookmarkDao;
import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.Book;
import com.epam.bookreader.entity.Bookmark;
import com.epam.bookreader.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "library", urlPatterns = "/library")
public class LibraryController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Author> authors = new ArrayList<>();
        List<Bookmark> bookmarks = new ArrayList<>();
        List<Book> books = new ArrayList<>();
        List<User> users = new ArrayList<>();

        User user = (User) req.getServletContext().getAttribute("user");
        try {
            authors = new AuthorDao().getAll();
            bookmarks = new BookmarkDao().getAll(user.getLogin());
            books = new BookDao().getAll();
            users = new UserDao().getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("authors", authors);
        req.setAttribute("bookmarks", bookmarks);
        req.setAttribute("books", books);
        req.setAttribute("users", users);

        req.getRequestDispatcher("/WEB-INF/templates/main.jsp").forward(req, resp);
    }
}
