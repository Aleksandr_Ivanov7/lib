package com.epam.bookreader.bisnesslayer;

import com.epam.bookreader.bisnesslayer.session.SessionUser;
import com.epam.bookreader.bisnesslayer.session.Validation;
import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.SQLException;

class LogIn {
    private Console console;

    LogIn(Console console) throws SQLException {
        this.console = console;
        enter();
    }

    private void enter() throws SQLException {
        for(int i = 0; i < 4; i++) {
            if(1 <= i) console.printToConsole("Enter login and password again. Осталось " + (4 - i) + " попытоки");

            String login = console.inputValueInParam("You login:");
            String password = console.inputValueInParam("You password:");
            User tempUser = new User(
                    login,
                    DigestUtils.md5Hex(password));
            if (Validation.isValidLogin(tempUser.getLogin()) && Validation.isValidPassword(tempUser.getPassword())) {
                User user = new UserDao().get(tempUser);
                if (user != null) new SessionUser(console, user);
            }else console.printToConsole("Not valid Login or user");
        }
        console.printToConsole("You don't have try");
    }
}
