package com.epam.bookreader.entity;

public class User implements Entity {
    private String userUUID;
    private String login;
    private String password;
    private Role role;
    private boolean active;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User(String userUUID, String login, String password, Role role, boolean active) {
        this.userUUID = userUUID;
        this.login = login;
        this.password = password;
        this.role = role;
        this.active = active;
    }

    public User(String login, String password, Role role, boolean active) {
        this.userUUID = setUUID();
        this.login = login;
        this.password = password;
        this.role = role;
        this.active = active;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public boolean isActive() {
        return active;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return String.format("[uuid] %10s [login] %-10s [admin] %-5s",
                userUUID.substring(0, 8) + "...", login, role.getName());
    }
}