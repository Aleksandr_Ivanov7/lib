package com.epam.bookreader.controller.author;

import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/authors/delete")
public class AuthorServletDelete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuid = req.getParameter("uuid");
        try {
            new AuthorDao().delete(uuid);
            User user = (User) req.getServletContext().getAttribute("user");
            Logger.createLog(user.getLogin(),"Author delete", uuid);
            Notification.addNotify(req,"Author successfully delete!", Notification.Type.SUCCESS);
        } catch (SQLException e) {
            Notification.addNotify(req,"Error! Author don't delete!" + e, Notification.Type.DANGER);
        }
        resp.sendRedirect(req.getContextPath() + "/library");
    }
}
