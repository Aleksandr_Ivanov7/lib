package com.epam.bookreader.dao;

import com.epam.bookreader.connection.ConnectionDB;
import com.epam.bookreader.entity.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LogDao {
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public LogDao() {
        this.con = new ConnectionDB().getConnection("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@localhost:1521:xe",
                "SOUTHWIND", "1");
    }

    public LogDao(Connection con) {
        this.con = con;
    }

    protected Log fillForm(ResultSet rs) throws SQLException {
        return new Log(
                rs.getString("logID"),
                rs.getString("login"),
                rs.getString("event"),
                rs.getString("recordID"),
                rs.getDate("eventTime")
        );
    }

    public boolean save(Log log) throws SQLException {
        ps = con.prepareStatement("INSERT INTO Logs VALUES (?, ?, ?, ?, ?)");

        ps.setString(1, log.getLogUUID());
        ps.setString(2, log.getLogin());
        ps.setString(3, log.getEvent());
        ps.setString(4, log.getRecordUUID());
        ps.setDate(5, log.getEventTime());

        return ps.execute();
    }

    public List<Log> getAll(String login) throws SQLException {
        List<Log> logs = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM Logs WHERE login = ?");
        ps.setString(1, login);
        rs = ps.executeQuery();
        while (rs.next()) logs.add(fillForm(rs));
        return logs;
    }

    public List<Log> getCountLogs(String login, int count) throws SQLException {
        List<Log> logs = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM Users WHERE login = ? ORDER BY dob DESC " +
                "FETCH FIRST ? ROWS ONLY");
        ps.setString(1, login);
        ps.setInt(2, count);
        rs = ps.executeQuery();
        while (rs.next()) logs.add(fillForm(rs));
        return logs;
    }
}
