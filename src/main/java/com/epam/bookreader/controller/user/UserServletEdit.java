package com.epam.bookreader.controller.user;

import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.AdminDao;
import com.epam.bookreader.dao.RoleDao;
import com.epam.bookreader.entity.Role;
import com.epam.bookreader.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/user/edit")
public class UserServletEdit extends HttpServlet {
    private User user;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuid = req.getParameter("uuid");
        try {
            user = new AdminDao().get(uuid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("user", user);
        req.getRequestDispatcher("/WEB-INF/templates/users/user_edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Role role = null;
        try {
            role = new RoleDao().getRoleByName(req.getParameter("role"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        User user = new User(
                req.getParameter("uuid"),
                req.getParameter("login"),
                req.getParameter("password").isEmpty() ?
                        this.user.getPassword() : DigestUtils.md5Hex(req.getParameter("password")),
                role,
                req.getParameter("active") != null);
        try {
            new AdminDao().update(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Logger.createLog(user.getLogin(),"User baned! ", "none");
        Notification.addNotify(req, "User successfully baned!", Notification.Type.SUCCESS);
        resp.sendRedirect(req.getContextPath() + "/library");
    }
}
