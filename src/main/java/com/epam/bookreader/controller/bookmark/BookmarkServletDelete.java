package com.epam.bookreader.controller.bookmark;

import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.BookmarkDao;
import com.epam.bookreader.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/bookmarks/delete")
public class BookmarkServletDelete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuid = req.getParameter("uuid");
        try {
            new BookmarkDao().delete(uuid);
            User user = (User) req.getServletContext().getAttribute("user");
            Logger.createLog(user.getLogin(),"Bookmark delete", uuid);
            Notification.addNotify(req, "Bookmark successfully delete!", Notification.Type.SUCCESS);
        } catch (SQLException e) {
            Notification.addNotify(req, "Error! Bookmark don't delete! " + e, Notification.Type.DANGER);
        } finally {
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }
}
