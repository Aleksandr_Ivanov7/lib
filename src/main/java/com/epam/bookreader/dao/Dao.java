package com.epam.bookreader.dao;

import java.sql.SQLException;
import java.util.List;

public interface Dao<T> {
    String ORACLE_DATE_FORMAT = "yyyy-mm-dd";

    T get(String id) throws SQLException;

    List<T> getAll() throws SQLException;

    boolean save(T t) throws SQLException;

    boolean delete(T t) throws SQLException;
}