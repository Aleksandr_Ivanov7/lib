package com.epam.bookreader.controller.book;

import com.epam.bookreader.bisnesslayer.logic.parser.ParserCSV;
import com.epam.bookreader.bisnesslayer.logic.parser.ParserJSON;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStreamReader;

@MultipartConfig
@WebServlet(urlPatterns = "/books/upload")
public class BookServletUploadFile extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/books/books_upload_from_file.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part file = req.getPart("file");
        InputStreamReader inputStreamReader = new InputStreamReader(file.getInputStream());
        if(file.getSubmittedFileName().matches("\\w+.json")) {
            ParserJSON json = new ParserJSON();
            json.loadToDB(json.parseFromStream(inputStreamReader));
        }else {
            ParserCSV csv = new ParserCSV();
            csv.loadToDB(csv.parseFromStream(inputStreamReader));
        }
        resp.sendRedirect(req.getContextPath() + "/library");
    }
}