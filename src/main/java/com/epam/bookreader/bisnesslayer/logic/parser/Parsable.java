package com.epam.bookreader.bisnesslayer.logic.parser;

import com.epam.bookreader.entity.Entity;

import java.util.List;

public interface Parsable<T extends Entity> {
    List<T> parseFromFile(String file);
    void loadToDB(List list);
}
