package com.epam.bookreader.controller.book;

import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.Book;
import com.epam.bookreader.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/books/add")
public class BookServletAdd extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/books/book_add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Author author = new Author(req.getParameter("author-name").trim(),
                req.getParameter("author-second-name").trim(),
                req.getParameter("author-last-name").trim(),
                Date.valueOf(req.getParameter("author-date-born")));
        Book book = new Book(
                req.getParameter("isbn").trim(),
                req.getParameter("book-name").trim(),
                req.getParameter("publisher").trim(),
                Integer.parseInt(req.getParameter("release-year").trim()),
                Integer.parseInt(req.getParameter("page-count").trim()),
                true,
                author
        );
        try {
            new AuthorDao().save(author);
            new BookDao().save(book);
            User user = (User) req.getServletContext().getAttribute("user");
            Logger.createLog(user.getLogin(), "Book add", book.getBookUUID());
            Logger.createLog(user.getLogin(), "Author add", author.getAuthorUUID());
            Notification.addNotify(req, "Book and author successfully added!", Notification.Type.SUCCESS);
        } catch (SQLException e) {
            Notification.addNotify(req, "Error! Book and author don't added! " + e, Notification.Type.DANGER);
        } finally {
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }
}
