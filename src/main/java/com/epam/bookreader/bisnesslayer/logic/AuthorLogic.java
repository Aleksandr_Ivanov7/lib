package com.epam.bookreader.bisnesslayer.logic;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.bisnesslayer.session.command.Command;
import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.dao.LogDao;
import com.epam.bookreader.entity.Author;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class AuthorLogic extends Option {
    private Console console;


    public AuthorLogic(Console console) {
        this.console = console;
    }

    public void go(String[] commands) throws SQLException {
        if (2 <= commands.length)
            switch (commands[1]) {
                case "add":
                    add();
                    break;
                case "rm":
                    remove();
                    break;
                case "get":
                    get();
                    break;
                case "find":
                    search();
                    break;
                default:
                    Command.messageNoRightCommand();
            }

    }

    public Author add() {
        try {
            console.printToConsole("Adding Author...");
            Author author = new Author(
                    console.inputValueInParam("Enter name author:"),
                    console.inputValueInParam("Enter second name author:"),
                    console.inputValueInParam("Enter last name author:"),
                    createDate());
            AuthorDao authorDao = new AuthorDao();
            if (!authorDao.isExistAuthor(author)) {
                authorDao.save(author);
                console.printToConsole("Author create!");
                new LogDao().save(LogLogic.createLog("Author create",
                        author.getAuthorUUID()));
            }
            return author;
        } catch (SQLException e) {
            console.printToConsoleError(e);
            return null;
        }
    }

    private Date createDate() {
        String dateBorn = console.inputValueInParam("Enter author birthday in format < yyyy-mm-dd > :");
        Date date = null;
        try {
            date = Date.valueOf(dateBorn);
        }catch (IllegalArgumentException e){
            console.printToConsole("Illegal date. ");
            createDate();
        }
        return date;
    }

    private void remove() throws SQLException {
        search();
        Author author = (Author) super.getEntity();
        //make one transaction
        new AuthorDao().delete(author);
        new BookDao().delete(author);
        console.printToConsole("Delete author and relationship books");
        new LogDao().save(LogLogic.createLog("Delete author and relationship books",
                author.getAuthorUUID()));
    }

    private List<Author> search() throws SQLException {
        List<Author> list = new AuthorDao().getAll(console.inputValueInParam("Enter part of name author : "));
        super.printListEntity(list, console);
        return list;
    }

    public List<Author> get() throws SQLException {
        List<Author> list = new AuthorDao().getAll();
        super.printListEntity(list, console);
        return list;
    }
}
