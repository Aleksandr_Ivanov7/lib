function view(role) {
    var tabBooks = document.getElementById('pills-books-tab');
    var tadBooksData = document.getElementById('pills-books');
    var tabUsers = document.getElementById('pills-users-tab');
    var tadUsersData = document.getElementById('pills-users');

    if (role === 'user' || role === 'admin') {
        tabBooks.classList.add('active');
        tabBooks.setAttribute('aria-selected', 'true');
        tadBooksData.classList.add('active');
        tadBooksData.classList.add('show');
        tabUsers.classList.remove('active');
        tabUsers.setAttribute('aria-selected', 'false');
        tadUsersData.classList.remove('active');
        tadUsersData.classList.remove('show');
    }else if (role === 'moderator'){
        tabUsers.classList.add('active');
        tabUsers.setAttribute('aria-selected', 'true');
        tadUsersData.classList.add('active');
        tadUsersData.classList.add('show');
        tabBooks.classList.remove('active');
        tabBooks.setAttribute('aria-selected', 'false');
        tadBooksData.classList.remove('active');
        tadBooksData.classList.remove('show');
    }
}