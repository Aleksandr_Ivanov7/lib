package com.epam.bookreader.dao;

import com.epam.bookreader.connection.ConnectionDB;
import com.epam.bookreader.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AdminDao extends UserDao {
    private Connection con;
    private PreparedStatement ps;

    public AdminDao() {
        this.con = new ConnectionDB().getConnection(
                "oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@localhost:1521:xe",
                "SOUTHWIND", "1");
    }

    public boolean delete(String login) throws SQLException {
        ps = con.prepareStatement("UPDATE Users SET active = 0 WHERE login = ?");
        ps.setString(1, login);
        return ps.execute();
    }

    public boolean update(User user) throws SQLException {
        ps = con.prepareStatement("UPDATE Users SET login = ?, password = ?, roleId = ?, active = ? WHERE userId = ?");
        ps.setString(1, user.getLogin());
        ps.setString(2, user.getPassword());
        ps.setString(3, user.getRole().getRoleUUID());
        ps.setInt(4, user.isActive() ? 1 : 0);
        ps.setString(5, user.getUserUUID());
        return ps.execute();
    }
}
