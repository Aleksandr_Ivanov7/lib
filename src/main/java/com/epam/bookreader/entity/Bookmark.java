package com.epam.bookreader.entity;

public class Bookmark implements Entity{
    private String uuid;
    private User user;
    private Book book;
    private int page;
    private boolean active;

    public Bookmark(String uuid, User user, Book book, int page, boolean active) {
        if(uuid == null) setUUID();
        else this.uuid = uuid;
        this.user = user;
        this.book = book;
        this.page = page;
        this.active = active;
    }

    public Bookmark(User user, Book book, int page, boolean active) {
        this.uuid = setUUID();
        this.user = user;
        this.book = book;
        this.page = page;
        this.active = active;
    }

    public String getUuid() {
        return uuid;
    }

    public User getUser() {
        return user;
    }

    public Book getBook() {
        return book;
    }

    public int getPage() {
        return page;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        return String.format("[uuid] %10s [login] %10s [isbn] %10s [page] %4s",
                uuid.substring(0,8)+"...", user.getLogin(), book.getBookName(), page);
    }
}
