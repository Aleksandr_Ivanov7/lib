package com.epam.bookreader.bisnesslayer.logic.parser;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ParserCSV implements Parsable {
    private static final Logger log = LogManager.getLogger(Console.class);

    private List<Book> getList(String csvString){
        List<Book> books = new ArrayList<>();
        String[] arrayBook = csvString.split("\n");
        log.debug(arrayBook);
        log.info("Start loading csv file\n");
        for (int i = 1; i < arrayBook.length; i++) {
            String[] book = arrayBook[i].split("[^\\s],[^\\s]");
            log.debug(book);
            book = enumeration(book);
            try {
                books.add(new Book(book[3], book[0], book[4], Integer.parseInt(book[1]), Integer.parseInt(book[2]), true,
                        new Author(book[6], book[7], book[5], new Date(new SimpleDateFormat("dd.MM.yyyy")
                                .parse(book[8]).getTime()))));
            } catch (ParseException e) {
                log.error(e);
            }
        }
        log.debug(books);
        return books;
    }
    public List<Book> parseFromFile(String file) {
        try {
            return getList(IOUtils.toString(new FileReader(file)));
        } catch (IOException e) {
            log.error(e);
            return Collections.emptyList();
        }
    }

    public List<Book> parseFromStream(InputStreamReader stream) {
        try {
            return getList(IOUtils.toString(stream));
        } catch (IOException e) {
            log.error(e);
            return Collections.emptyList();
        }
    }

    public void loadToDB(List books) {
        Iterator<Book> iter = books.iterator();
        while (iter.hasNext()) {
            Book book = iter.next();
            Author author = book.getAuthor();
            book.setBookUUID(book.setUUID());
            author.setAuthorUUID(author.setUUID());
            try {//make one transaction
                new AuthorDao().save(book.getAuthor());
                new BookDao().save(book);

            } catch (SQLException e) {
                log.error(e);
            }
        }
        log.info("end load csv file\n");
    }

    private String[] enumeration(String[] arrayData) {
        for (int i = 0; i < arrayData.length; i++) {
            arrayData[i] = deleteQuotes(arrayData[i]);
        }
        return arrayData;
    }

    private static String deleteQuotes(String data) {
        return data.replaceAll("\"", "");
    }
}
