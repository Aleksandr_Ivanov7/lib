package com.epam.bookreader.controller.book;

import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.entity.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/books/delete")
public class BookServletDelete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String uuid = req.getParameter("uuid");
        try {
            new BookDao().delete(uuid);
            User user = (User) req.getServletContext().getAttribute("user");
            Logger.createLog(user.getLogin(), "Book delete", uuid);
            Notification.addNotify(req, "Book successfully Delete!", Notification.Type.SUCCESS);
        } catch (SQLException e) {
            Notification.addNotify(req, "Error! Book and author don't added! " + e, Notification.Type.DANGER);
        } finally {
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }
}
