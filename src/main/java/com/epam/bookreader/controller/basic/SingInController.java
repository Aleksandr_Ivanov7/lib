package com.epam.bookreader.controller.basic;

import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

@WebServlet(name = "SingIn", urlPatterns = "/sing_in")
public class SingInController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/sing_in.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String login = req.getParameter("login");
        String password = DigestUtils.md5Hex(req.getParameter("password"));

        try {
            User user = new UserDao().get(new User(login, password));
            if(user != null){
                req.getServletContext().setAttribute("user", user);
                Cookie cookie = new Cookie("token", UUID.randomUUID().toString());
                resp.addCookie(cookie);
                resp.sendRedirect(req.getContextPath() + "/library");
            }else resp.sendRedirect(req.getRequestURI());
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}
