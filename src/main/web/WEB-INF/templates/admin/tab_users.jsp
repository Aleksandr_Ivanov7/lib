<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${user.role.name == 'admin' || user.role.name == 'moderator'}">
    <li class="nav-item">
        <a class="nav-link" id="pills-users-tab" data-toggle="pill" href="#pills-users"
           role="tab" aria-controls="pills-users" aria-selected="false">Users</a>
    </li>
</c:if>