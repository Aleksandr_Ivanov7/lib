package com.epam.bookreader.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadProperties {
    private final static String pathProperties = "src/main/resources/config.properties";

    public static String getProperties(String nameProperties){
        try (InputStream input = new FileInputStream(pathProperties)) {
            Properties prop = new Properties();
            prop.load(input);
            return prop.getProperty(nameProperties);
        } catch (IOException e) {
            return "";
        }
    }
}