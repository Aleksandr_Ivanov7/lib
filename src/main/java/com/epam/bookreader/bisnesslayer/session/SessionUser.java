package com.epam.bookreader.bisnesslayer.session;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.bisnesslayer.session.command.Command;
import com.epam.bookreader.entity.User;

import java.sql.SQLException;

public class SessionUser {
    public static User user;

    public SessionUser(Console console, User user) throws SQLException {
        SessionUser.user = user;
        if (user.isActive()) {
            console.printToConsole("You enter as " + user.getRole().getName() + "( " + user.getLogin() + " )");
            Command command = new Command(console);
            console.printToConsole(command.toString());
            command.distribution();
        } else {
            console.printToConsole("Access denied. You was banned");
            System.exit(0);
        }
    }
    public static boolean isAdmin() {
        return user.getRole().getName().equalsIgnoreCase("admin");
    }
}