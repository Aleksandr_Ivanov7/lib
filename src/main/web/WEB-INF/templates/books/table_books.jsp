<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
    <table id="table-books" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Publisher</th>
            <th scope="col">Year</th>
            <th scope="col">Count pages</th>
            <th scope="col">Author</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <%--    <c:forEach var="author" items="${authors}">--%>
        <% int i = 0;%>
        <c:forEach var="book" items="${books}">
            <c:if test="${book.active}">
            <tr>
                <th scope="row"><%=++i%>
                </th>
                <td><c:out value="${book.isbn}"/></td>
                <td><c:out value="${book.bookName}"/></td>
                <td><c:out value="${book.publisher}"/></td>
                <td><c:out value="${book.releaseYear}"/></td>
                <td><c:out value="${book.pageCount}"/></td>
                <td>
                    <c:out value="${book.author.name}"/>
                    <c:out value="${book.author.secondName}"/>
                    <c:out value="${book.author.lastName}"/>
                </td>
                <td><a href="${pageContext.request.contextPath}/books/delete?uuid=${book.bookUUID}">delete</a></td>
            </tr>
            </c:if>
        </c:forEach>
        </tbody>
    </table>
</div>