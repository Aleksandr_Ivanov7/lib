package com.epam.bookreader.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

public class Author implements Entity {
    private String authorUUID;
    @JsonProperty("name")
    private String name;
    @JsonProperty("secondName")
    private String secondName;
    @JsonProperty("lastName")
    private String lastName;

    @JsonFormat(pattern = "dd.MM.yyyy")
    @JsonProperty("dob")
    private Date dob;

    private boolean active;
    public Author(String authorUUID, String name, String secondName, String lastName, Date dob, boolean active) {
        this.authorUUID = authorUUID != null ? authorUUID : setUUID();
        this.name = name;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dob = dob;
        this.active = active;
    }

    public Author(String name, String secondName, String lastName, Date dob) {
        this.authorUUID = setUUID();
        this.name = name;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dob = dob;
        this.active = true;
    }

    public Author() {

    }

    public String getName() {
        return name;
    }

    public Date getDob() {
        return dob == null ? Date.valueOf("") : dob;
    }


    public String getAuthorUUID() {
        return authorUUID == null ? "" : authorUUID;
    }

    public String getLastName() {
        return lastName == null ? "" : lastName;
    }

    public String getSecondName() {
        return secondName == null ? "" : secondName;
    }

    public boolean isActive() {
        return active;
    }

    public void setAuthorUUID(String uuid) {
        authorUUID = uuid;
    }

    @Override
    public String toString() {
        return String.format("[uuid] %10s [name] %-10s [second name] %-10s [last name] %-10s [birthday] %-10s",
                authorUUID.substring(0, 8) + "...", name, secondName, lastName,
                dob == null ? "" : dob.toString().substring(0, 10));
    }

}