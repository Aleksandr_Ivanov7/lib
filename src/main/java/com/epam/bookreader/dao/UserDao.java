package com.epam.bookreader.dao;

import com.epam.bookreader.connection.ConnectionDB;
import com.epam.bookreader.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements Dao<User> {
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public UserDao() {
        this.con = new ConnectionDB().getConnection("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@localhost:1521:xe",
                "SOUTHWIND", "1");
    }

    protected User fillForm(ResultSet rs) throws SQLException {
        return new User(
                rs.getString("userId"),
                rs.getString("login"),
                rs.getString("password"),
                new RoleDao(con).getRoleByUUID(rs.getString("roleId")),
                rs.getBoolean("active")
        );
    }

    public boolean isExistUser(String login, String password) throws SQLException {
        ps = con.prepareStatement("SELECT login, password FROM Users WHERE login = ? AND password = ?");
        ps.setString(1, login);
        ps.setString(2, password);
        return 0 < ps.executeUpdate();
    }

    public boolean isExistUser(String login) throws SQLException {
        ps = con.prepareStatement("SELECT login, password FROM Users WHERE login = ?");
        ps.setString(1, login);
        return 0 < ps.executeUpdate();
    }

    @Override
    public User get(String uuid) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM Users WHERE userid LIKE ?");
        ps.setString(1, uuid + "%");
        rs = ps.executeQuery();
        User user = null;
        while (rs.next()) user = fillForm(rs);
        return user;
    }

    public User getByLogin(String login) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM Users WHERE login = ?");
        ps.setString(1, login);
        rs = ps.executeQuery();
        User user = null;
        while (rs.next()) user = fillForm(rs);
        return user;
    }

    public User get(User user) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM Users WHERE login = ? AND password = ?");
        ps.setString(1, user.getLogin());
        ps.setString(2, user.getPassword());
        rs = ps.executeQuery();
        User newUser = null;
        while (rs.next()) newUser = fillForm(rs);
        return newUser;
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try(ResultSet rs = con.createStatement().executeQuery("SELECT * FROM Users")) {
            while (rs.next()) users.add(fillForm(rs));
        } catch (SQLException ignore){ }
        return users;
    }

    @Override
    public boolean save(User user) throws SQLException {
        ps = con.prepareStatement("INSERT INTO Users VALUES (?, ?, ?, ?, ?)");

        ps.setString(1, user.getUserUUID());
        ps.setString(2, user.getLogin());
        ps.setString(3, user.getPassword());
        ps.setString(4, user.getRole().getRoleUUID());
        ps.setInt(5, user.isActive() ? 1 : 0);

        return ps.execute();
    }

    @Override
    public boolean delete(User user) {
        return false;
    }

}
