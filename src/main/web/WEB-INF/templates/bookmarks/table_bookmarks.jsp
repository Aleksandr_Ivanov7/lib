<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
    <table id="table-bookmarks" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Book</th>
            <th scope="col">Bookmark(закладка)</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <% int i = 0;%>
        <c:forEach var="bookmark" items="${bookmarks}">
            <c:if test="${bookmark.active}">
            <tr>
                <th scope="row"><%=++i%></th>
                <td><c:out value="${bookmark.book.bookName}"/></td>
                <td><c:out value="${bookmark.page}"/></td>
                <td><a href="${pageContext.request.contextPath}/bookmarks/delete?uuid=${bookmark.uuid}">delete</a></td>
            </tr>
            </c:if>
        </c:forEach>
        </tbody>
    </table>
</div>
