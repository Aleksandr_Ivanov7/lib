package com.epam.bookreader.controller.log;

import com.epam.bookreader.dao.LogDao;
import com.epam.bookreader.entity.Log;

import java.sql.SQLException;

public class Logger {
    public static void createLog(String login,String event, String uuid){
        try {
            new LogDao().save(
                    new Log(login,event,uuid)
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
