<script
        src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js">
</script>
<section>
<form id="author-put">
    <table class="table">
        <tr>
            <td><label>Name</label></td>
            <td><input name="name"></td>
        </tr>
        <tr>
            <td><label>Second name</label></td>
            <td><input name="second-name"></td>
        </tr>
        <tr>
            <td>Last name</td>
            <td><input name="last-name"></td>
        </tr>
        <tr>
            <td><label>Date born</label></td>
            <td><input name="date-born" type="date"></td>
        </tr>
        <tr>
            <td><button type="submit" class="btn btn-primary" value="Submit">Put</button></td>
        </tr>
    </table>
</form>
    <div id="msg"></div>
</section>
<script>
    $("#author-put").submit(function(event){
        event.preventDefault();
        var $form = $(this);
        //var userId = $form.find('input[name="authorId"]').val();
        var url = 'http://localhost:8081/my-app/authors/';
        var authorName = $form.find('input[name="name"]').val();

        $.ajax({
            type : 'PUT',
            url : url,
            contentType: 'application/json',
            data : JSON.stringify({name: authorName}),
            success : function(data, status, xhr){
                window.location.replace("http://localhost:8081/my-app/library");
            },
            error: function(xhr, status, error){
                $('#msg').html('<span style=\'color:red;\'>'+error+'</span>')
            }
        });
    });
</script>