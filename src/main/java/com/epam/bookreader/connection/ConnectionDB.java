package com.epam.bookreader.connection;

import com.epam.bookreader.configuration.LoadProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
    private static Connection connection;

    public Connection getConnection() {
        try {
            Class.forName(LoadProperties.getProperties("db.driver"));
            connection = DriverManager.getConnection(
                    LoadProperties.getProperties("db.url"),
                    LoadProperties.getProperties("db.user"),
                    LoadProperties.getProperties("db.password"));
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            this.close();
        }
        return connection;
    }
    public Connection getConnection(String driver, String url, String user, String password) {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            this.close();
        }
        return connection;
    }

    private void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }
}