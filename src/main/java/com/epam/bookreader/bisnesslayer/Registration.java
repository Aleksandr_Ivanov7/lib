package com.epam.bookreader.bisnesslayer;

import com.epam.bookreader.bisnesslayer.session.SessionUser;
import com.epam.bookreader.bisnesslayer.session.Validation;
import com.epam.bookreader.dao.RoleDao;
import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.SQLException;

class Registration {
    private Console console;

    @Deprecated
    Registration(Console console) throws SQLException {
        this.console = console;
        enter();
    }

    private void enter() throws SQLException {
        console.printToConsole("[Reg] Start registration!");
        for (int i = 0; i < 15; i++) {
            if (1 <= i) console.printToConsole("Не валидные данные. Осталось " + (15 - i) + " попытоки");
            String login = console.inputValueInParam("[Reg] Enter login : ");
            String password = console.inputValueInParam("[Reg] Enter password : ");

            if (Validation.isValidLogin(login) && Validation.isValidPassword(password)) {
                User user = new User(login, DigestUtils.md5Hex(password), new RoleDao().getRoleByName("user"), true);
                new UserDao().save(user);
                new SessionUser(console, user);
            }
        }
    }
}
