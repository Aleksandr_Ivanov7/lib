package com.epam.bookreader;

import com.epam.bookreader.bisnesslayer.Console;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        Console c = new Console();
        c.startApp();
    }
}