<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style><%@include file="../css/style_sing_in.css"%></style>
<html>
<head>
    <title>Sing in</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <a class="navbar-brand" href="#">Library</a>
        </nav>
    </header>
    <section class="sing-in">
        <form action="sing_in" method="post">
            <div class="form-group">
                <label>Login</label>
                <input name="login" type="text">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input name="password" type="password">
            </div>
            <button type="submit" class="btn btn-primary">Sing in</button>
        </form>
    </section>
    <footer class="page-footer font-small indigo">
        <div class="footer-copyright text-center py-3">© 2020 Copyright:
            <a href="#"> MySite</a>
        </div>
    </footer>
</body>
</html>