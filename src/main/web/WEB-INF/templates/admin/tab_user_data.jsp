<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${user.role.name == 'admin' || user.role.name == 'moderator'}">
        <div class="tab-pane fade" id="pills-users" role="tabpanel"
             aria-labelledby="pills-users-tab">
            <a class="btn btn-outline-primary" role="button" href="users/add">add user</a>
            <div class="active-cyan-4 mb-4 search-field">
                <input id="search-users" class="form-control " type="text" placeholder="Search" aria-label="Search" onkeyup="search(id, 'table-users')">
            </div>
            <jsp:include page="../users/table_users.jsp"/>
        </div>
</c:if>