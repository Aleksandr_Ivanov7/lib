package com.epam.bookreader.bisnesslayer.logic;

import com.epam.bookreader.bisnesslayer.session.SessionUser;
import com.epam.bookreader.entity.Log;

public class LogLogic {
    public static Log createLog(String event, String recordUUID){
        return new Log(
                SessionUser.user.getLogin(),
                event,
                recordUUID
        );
    }
}
