package com.epam.bookreader.entity;

import java.util.UUID;

public interface Entity {

    boolean isActive();

    default String setUUID(){
        return UUID.randomUUID().toString();
    }
}
