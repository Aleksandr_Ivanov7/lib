package com.epam.bookreader.dao;

import com.epam.bookreader.connection.ConnectionDB;
import com.epam.bookreader.entity.Author;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuthorDao implements Dao<Author> {
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public AuthorDao(Connection con) {
        this.con = con;
    }

    public AuthorDao() {
        this.con = new ConnectionDB().getConnection("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@localhost:1521:xe",
                "SOUTHWIND", "1");
    }

    protected Author fillForm(ResultSet rs) throws SQLException {
        return new Author(
                rs.getString("authorId"),
                rs.getString("name"),
                rs.getString("secondName"),
                rs.getString("lastName"),
                rs.getDate("dob"),
                rs.getBoolean("active")
        );
    }

    public boolean isExistAuthor(Author author) throws SQLException {
        ps = con.prepareStatement("SELECT name FROM Authors WHERE name = ?");
        ps.setString(1, author.getSecondName());
        return 0 < ps.executeUpdate();
    }

    @Override
    public Author get(String uuid) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM Authors WHERE authorId = ?");
        ps.setString(1, uuid);
        ResultSet rs = ps.executeQuery();
        Author author = null;
        while(rs.next()) author = fillForm(rs);
        return author;
    }

    @Override
    public List<Author> getAll() throws SQLException {
        List<Author> authors = new ArrayList<>();
        rs = con.createStatement().executeQuery("SELECT * FROM Authors");
        while (rs.next()) authors.add(fillForm(rs));
        return authors;
    }

    public List<Author> getAll(String name) throws SQLException {
        List<Author> authors = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM Authors WHERE name LIKE ?");
        ps.setString(1, name + "%");
        rs = ps.executeQuery();
        while (rs.next()) authors.add(fillForm(rs));
        return authors;
    }

    @Override
    public boolean save(Author author) throws SQLException {
        ps = con.prepareStatement("INSERT INTO Authors VALUES (?, ?, ?, ?, ?, ?)");
        ps.setString(1, author.getAuthorUUID());
        ps.setString(2, author.getName());
        ps.setString(3, author.getSecondName());
        ps.setString(4, author.getLastName());
        ps.setDate(5, author.getDob());
        ps.setInt(6, author.isActive() ? 1 : 0);
        return ps.execute();
    }

    @Override
    public boolean delete(Author author) throws SQLException {
        ps = con.prepareStatement("UPDATE Authors SET active = 0 WHERE authorId = ?");
        ps.setString(1, author.getAuthorUUID());
        return ps.execute();
    }


    public boolean delete(String uuid) throws SQLException {
        ps = con.prepareStatement("UPDATE Authors SET active = 0 WHERE authorId = ?");
        ps.setString(1, uuid);
        return ps.execute();
    }
}