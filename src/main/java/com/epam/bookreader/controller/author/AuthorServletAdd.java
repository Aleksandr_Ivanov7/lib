package com.epam.bookreader.controller.author;

import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/authors/add")
public class AuthorServletAdd extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/authors/author_add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        String name = req.getParameter("name").trim();
        String lastName = req.getParameter("last-name").trim();
        String secondName = req.getParameter("second-name").trim();
        Date date = Date.valueOf(req.getParameter("date-born"));
        Author author = new Author(name, lastName, secondName, date);
        try {
            new AuthorDao().save(author);
            User user = (User) req.getServletContext().getAttribute("user");
            Logger.createLog(user.getLogin(),"Author add", author.getAuthorUUID());
            Notification.addNotify(req,"Author successfully added!", Notification.Type.SUCCESS);
        } catch (SQLException e) {
            Notification.addNotify(req,"Error! Author don't added! " + e, Notification.Type.DANGER);
        }finally {
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }
}
