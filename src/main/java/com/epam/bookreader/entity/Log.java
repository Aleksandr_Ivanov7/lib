package com.epam.bookreader.entity;

import java.sql.Date;

public class Log implements Entity{
    private String logUUID;
    private String login;
    private String event;
    private String recordUUID;
    private Date eventTime;

    public Log(String logUUID, String login, String event, String recordId, Date eventTime) {
        this.logUUID = logUUID;
        this.login = login;
        this.event = event;
        this.recordUUID = recordId;
        this.eventTime = eventTime;
    }

    public Log(String login, String event, String recordUUID, Date eventTime) {
        this.logUUID = setUUID();
        this.login = login;
        this.event = event;
        this.recordUUID = recordUUID;
        this.eventTime = eventTime;
    }

    public Log(String login, String event, String recordUUID) {
        this.logUUID = setUUID();
        this.login = login;
        this.event = event;
        this.recordUUID = recordUUID;
        this.eventTime = new Date(System.currentTimeMillis());
    }


    @Override
    public String toString() {
        return String.format("[logUUID] %10s [login] %10s [event] %10s [recordId] %8s [when] %8s",
                logUUID.substring(0,8)+"...", login, event, recordUUID.substring(0,8)+"...", eventTime.toString().substring(0,8));
    }

    public String getLogUUID() {
        return logUUID;
    }

    public String getLogin() {
        return login;
    }

    public String getEvent() {
        return event;
    }

    public String getRecordUUID() {
        return recordUUID;
    }

    public Date getEventTime() {
        return eventTime;
    }

    @Override
    public boolean isActive() {
        return true;
    }
}
