package com.epam.bookreader.bisnesslayer.logic.parser;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ParserJSON implements Parsable {
    private static final Logger log = LogManager.getLogger(Console.class);

    private List<Book> getList(Book[] jsonBook) {
        log.info("Load json file.\n");
        List<Book> books = new ArrayList<>();
        Collections.addAll(books, jsonBook);
        return books;
    }

    public List<Book> parseFromStream(InputStreamReader stream){
        try {
            return getList(new ObjectMapper().readValue(stream, Book[].class));
        } catch (IOException e) {
            log.error(e);
            return Collections.emptyList();
        }
    }

    public List<Book> parseFromFile(String path){
        try {
            return getList(new ObjectMapper().readValue(new File(path), Book[].class));
        } catch (IOException e) {
            log.error(e);
            return Collections.emptyList();
        }
    }

    public void loadToDB(List books) {
        //add uuid books ,authors
        Iterator<Book> iter = books.iterator();
        while (iter.hasNext()) {
            Book book = iter.next();
            Author author = book.getAuthor();

            book.setBookUUID(book.setUUID());
            book.setActive(true);
            author.setAuthorUUID(author.setUUID());

            try {//make one transaction
                new AuthorDao().save(book.getAuthor());
                new BookDao().save(book);
            } catch (SQLException e) {
                log.error(e);
            }
        }
        log.info("End load json file.\n");
    }
}
