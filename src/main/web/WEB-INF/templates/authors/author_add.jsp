<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title>Library</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<style><%@include file="../../css/form_add.css" %></style>
<jsp:include page="../header.jsp"/>
<section class="form-add">
    <form action="${pageContext.request.contextPath}/authors/add" method="post">
        <table class="table">
            <tr>
                <td><label>Name</label></td>
                <td><input name="name" type="text" required></td>
            </tr>
            <tr>
                <td><label>Second name</label></td>
                <td><input name="second-name" type="text" required></td>
            </tr>
            <tr>
                <td>Last name</td>
                <td><input name="last-name" type="text" required></td>
            </tr>
            <tr>
                <td><label>Date born</label></td>
                <td><input name="date-born" type="date" required></td>
            </tr>
            <tr>
                <td><button type="submit" class="btn btn-primary">Add</button></td>
                <td></td>
            </tr>
        </table>
    </form>
</section>

