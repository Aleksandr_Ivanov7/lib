<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>
    <title>Library</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<style>
    <%@include file="../../css/main.css" %>
</style>

<jsp:include page="../header.jsp"/>
<div class="container">
    <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <jsp:include page="../notify.jsp"/>
        <p class="user-info">Logs user: ${param.login}</p>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Event</th>
                <th scope="col">Time</th>
            </tr>
            </thead>
            <tbody>

            <% int i = 0;%>
            <c:set var="log.eventTime" value="<%=new java.util.Date()%>"/>
            <c:forEach var="log" items="${logs}">
                    <tr>
                        <th scope="row"><%=++i%></th>
                        <td><c:out value="${log.event}"/></td>
                        <td><fmt:formatDate type="both" value="${log.eventTime}"/></td>
                    </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>