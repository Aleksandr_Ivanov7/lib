<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
    <table id="table-authors" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Second name</th>
            <th scope="col">Last name</th>
            <th scope="col">Date born</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <% int i = 0;%>
        <c:set var="author.dob" value="<%=new java.util.Date()%>"/>
        <c:forEach var="author" items="${authors}">
            <c:if test="${author.active}">
            <tr>
                <th scope="row"><%=++i%></th>
                <td><c:out value="${author.name}"/></td>
                <td><c:out value="${author.secondName}"/></td>
                <td><c:out value="${author.lastName}"/></td>
                <td><fmt:formatDate type="date" value="${author.dob}"/></td>
                <td><a href="${pageContext.request.contextPath}/authors/delete?uuid=${author.authorUUID}">delete</a></td>
            </tr>
            </c:if>
        </c:forEach>
        </tbody>
    </table>
</div>