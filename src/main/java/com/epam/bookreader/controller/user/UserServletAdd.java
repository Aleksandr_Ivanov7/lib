package com.epam.bookreader.controller.user;

import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.RoleDao;
import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.Role;
import com.epam.bookreader.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/users/add")
public class UserServletAdd extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Role> roles = null;
        try {
            roles = new RoleDao().getAll();
        } catch (SQLException e) {
            Notification.addNotify(req, "Error! Roles don't added! " + e, Notification.Type.DANGER);
        } finally {
            req.setAttribute("roles", roles);
            req.getRequestDispatcher("/WEB-INF/templates/users/user_add.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("login");
        String password = DigestUtils.md5Hex(req.getParameter("password"));
        String role = req.getParameter("role");
        boolean active = (req.getParameter("active") != null);
        User webUser = (User) req.getServletContext().getAttribute("user");
        try {
            User user = new User(name, password, new RoleDao().getRoleByName(role), active);
            new UserDao().save(user);
            Logger.createLog(user.getLogin(), "User add", webUser.getUserUUID());
            Notification.addNotify(req, "User successfully added!", Notification.Type.SUCCESS);
        } catch (SQLException e) {
            Notification.addNotify(req, "Error! User don't added! " + e, Notification.Type.DANGER);
        } finally {
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }
}
