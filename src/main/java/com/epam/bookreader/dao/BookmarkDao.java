package com.epam.bookreader.dao;

import com.epam.bookreader.connection.ConnectionDB;
import com.epam.bookreader.entity.Bookmark;
import com.epam.bookreader.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookmarkDao implements Dao<Bookmark> {
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public BookmarkDao() {
        this.con = new ConnectionDB().getConnection("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@localhost:1521:xe",
                "SOUTHWIND", "1");
    }

    public BookmarkDao(Connection con) {
        this.con = con;
    }

    protected Bookmark fillForm(ResultSet rs) throws SQLException {
        return new Bookmark(rs.getString("bookmarkId"),
                new UserDao().getByLogin(rs.getString("login")),
                new BookDao().getBookOnISBN(rs.getString("isbn")),
                rs.getInt("page"),
                rs.getBoolean("active")
        );
    }

    public boolean isExistBookmark(Bookmark bookMark) throws SQLException {
        ps = con.prepareStatement("SELECT login FROM Bookmarks WHERE login = ?");
        ps.setString(1, bookMark.getUser().getLogin());
        return 0 < ps.executeUpdate();
    }

    @Override
    public Bookmark get(String uuid) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM BookMarks WHERE bookmarkId = ?");
        ps.setString(1, uuid);
        return fillForm(ps.executeQuery());
    }

    @Override
    public List<Bookmark> getAll() throws SQLException {
        return null;
    }

    public List<Bookmark> getAll(User user) throws SQLException {
        List<Bookmark> bookmarks = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM BookMarks WHERE login = ?");
        ps.setString(1, user.getLogin());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) bookmarks.add(fillForm(rs));
        return bookmarks;
    }

    public List<Bookmark> getAll(String login) throws SQLException {
        List<Bookmark> bookmarks = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM BookMarks WHERE login = ?");
        ps.setString(1, login);
        rs = ps.executeQuery();
        while (rs.next()) bookmarks.add(fillForm(rs));
        return bookmarks;
    }

    @Override
    public boolean save(Bookmark bookMark) throws SQLException {
        ps = con.prepareStatement("INSERT INTO BookMarks VALUES (?, ?, ?, ?, ?)");
        ps.setString(1, bookMark.getUuid());
        ps.setString(2, bookMark.getUser().getLogin());
        ps.setString(3, bookMark.getBook().getIsbn());
        ps.setInt(4, bookMark.getPage());
        ps.setInt(5, bookMark.isActive() ? 1 : 0);
        return ps.execute();
    }

    @Override
    public boolean delete(Bookmark bookMark) throws SQLException {
        ps = con.prepareStatement("UPDATE Bookmarks SET active = 0 WHERE bookMarkId = ?");
        ps.setString(1, bookMark.getUuid());
        return ps.execute();
    }

    public boolean delete(String uuid) throws SQLException {
        ps = con.prepareStatement("UPDATE Bookmarks SET active = 0 WHERE bookMarkId = ?");
        ps.setString(1, uuid);
        return ps.execute();
    }

}
