package com.epam.bookreader.controller.filter;

import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/authors/*", "/books/*", "/bookmarks/*"})
public class ModeratorFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        User user = (User) req.getServletContext().getAttribute("user");
        if (isNotModerator(user)) {
            chain.doFilter(request, response);
        } else {
            Notification.addNotify(req, "Access denied! ", Notification.Type.DANGER);
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }

    private boolean isNotModerator(User user) {
        return !user.getRole().getName().equalsIgnoreCase("moderator");
    }

    @Override
    public void init(FilterConfig filterConfig){
    }

    @Override
    public void destroy() {
    }
}
