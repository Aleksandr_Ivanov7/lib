package com.epam.bookreader.controller.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/authors/*", "/books/*", "/bookmarks/", "/library/*", "/users/*", "/user/*"})
public class AuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        Cookie[] cookies = req.getCookies();
        boolean isAccess = false;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token") && !cookie.getValue().isEmpty()) isAccess = true;
        }
        if (isAccess) chain.doFilter(request, response);
        else resp.sendRedirect(req.getContextPath() + "/sing_in");
    }

    @Override
    public void destroy() {
    }
}
