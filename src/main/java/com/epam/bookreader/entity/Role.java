package com.epam.bookreader.entity;

public class Role implements Entity{
    private String roleUUID;
    private String name;
    private boolean active;

    public Role(String roleUUID, String name, boolean active) {
        this.roleUUID = roleUUID;
        this.name = name;
        this.active = active;
    }

    public String getRoleUUID() {
        return roleUUID;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean isActive() {
        return active;
    }
}
