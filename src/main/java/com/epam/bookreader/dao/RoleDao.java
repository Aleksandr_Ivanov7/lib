package com.epam.bookreader.dao;

import com.epam.bookreader.connection.ConnectionDB;
import com.epam.bookreader.entity.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleDao {
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public RoleDao(Connection con) {
        this.con = con;
    }

    public RoleDao() {
        this.con = new ConnectionDB().getConnection("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@localhost:1521:xe",
                "SOUTHWIND", "1");
    }

    protected Role fillForm(ResultSet rs) throws SQLException {
        return new Role(
                rs.getString("roleId"),
                rs.getString("name"),
                rs.getBoolean("active")
        );
    }

    public Role getRoleByUUID(String uuid) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM Roles WHERE roleId = ?");
        ps.setString(1, uuid);
        rs = ps.executeQuery();
        Role role = null;
        while (rs.next()) role = fillForm(rs);
        return role;
    }

    public Role getRoleByName(String name) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM Roles WHERE name = ?");
        ps.setString(1, name);
        rs = ps.executeQuery();
        Role role = null;
        while (rs.next()) role = fillForm(rs);
        return role;
    }

    public List<Role> getAll() throws SQLException {
        List<Role> roles = new ArrayList<>();
        rs = con.createStatement().executeQuery("SELECT * FROM Roles");
        while (rs.next()) roles.add(fillForm(rs));
        return roles;
    }
}
