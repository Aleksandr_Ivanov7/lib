<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
    <c:when test="${type == 'SUCCESS'}">
        <div id="notify">
            <div class="alert alert-success" role="alert">
                <c:out value="${message}"/>
            </div>
        </div>
    </c:when>
    <c:when test="${type == 'INFO'}">
        <div id="notify">
            <div class="alert alert-info" role="alert">
                <c:out value="${message}"/>
            </div>
        </div>
    </c:when>
    <c:when test="${type == 'WARNING'}">
        <div id="notify">
            <div class="alert alert-warning" role="alert">
                <c:out value="${message}"/>
            </div>
        </div>
    </c:when>
    <c:when test="${type == 'DANGER'}">
        <div id="notify">
            <div class="alert alert-danger" role="alert">
                <c:out value="${message}"/>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div id="notify">
            <div class="alert" role="alert">
                &nbsp;
            </div>
        </div>
    </c:otherwise>
</c:choose>
<c:remove var="type" scope="session"/>
<c:remove var="message" scope="session"/>