package com.epam.bookreader.bisnesslayer.session.command;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.bisnesslayer.logic.AuthorLogic;
import com.epam.bookreader.bisnesslayer.logic.BookLogic;
import com.epam.bookreader.bisnesslayer.logic.BookmarkLogic;
import com.epam.bookreader.bisnesslayer.logic.UserLogic;
import com.epam.bookreader.bisnesslayer.logic.parser.ParserCSV;
import com.epam.bookreader.bisnesslayer.logic.parser.ParserJSON;
import com.epam.bookreader.bisnesslayer.session.SessionUser;
import com.epam.bookreader.configuration.LoadProperties;

import java.sql.SQLException;

public class Command {
    private static Console console;

    public Command(Console console) {
        Command.console = console;
    }

    public void distribution() throws SQLException {
        while (true) {
            console.printToConsole("Enter command:");
            String[] commands = console.inputValue(null).split("\\s");
            switch (commands[0]) {
                case "book":
                    new BookLogic(console).go(commands);
                    break;
                case "author":
                    new AuthorLogic(console).go(commands);
                    break;
                case "bookmark":
                    new BookmarkLogic(console).go(commands);
                    break;
                case "exit":
                    exit();
                    break;
                case "help":
                    console.printToConsole(toString());
                    break;
                case "json":
                    ParserJSON json = new ParserJSON();
                    json.loadToDB(json.parseFromFile(LoadProperties.getProperties("json.file")));
                    break;
                case "csv":
                    ParserCSV csv = new ParserCSV();
                    csv.loadToDB(csv.parseFromFile(LoadProperties.getProperties("csv.file")));
                    break;
                case "user":
                    new UserLogic(console).go(commands);
                    break;
                default:
                    messageNoRightCommand();
            }
        }
    }

    public static void messageNoRightCommand() {
        console.printToConsole("Command not recognized...");
    }

    private void exit() {
        console.printToConsole("You (" + SessionUser.user.getLogin() + ") is exit");
        System.exit(0);
    }

    @Override
    public String toString() {
        String userHelp =
                "\nList command:\n" +
                        "1) [help] - get list commands\n" +
                        "2) [exit] - exit from app\n" +
                        "3) [csv] - for load data from csv file to DB\n" +
                        "4) [json] - for load data from json file to DB\n" +
                        "5) [book rm/add/get] - Work with book\n" +
                        "6) [author rm/add/get] - Work with author\n" +
                        "7) [bookmark rm/add/get] - Work with bookmark\n" +
                        "8) [book find [year] - Find book on year\n" +
                        "              [years] - Find book After start year before end dates\n" +
                        "              [title] - Find on title book\n" +
                        "              [name] - Find on name Author\n" +
                        "              [ISBN]] - Find on ISBN book;\n";
        String adminHelp = "" +
                "9) [user [create] - Create user\n" +
                "         [ban] - Banning user\n" +
                "         [log] - Show last history user\n" +
                "         [get]] - Show all users";
        return SessionUser.isAdmin() ? userHelp + adminHelp : userHelp;
    }
}