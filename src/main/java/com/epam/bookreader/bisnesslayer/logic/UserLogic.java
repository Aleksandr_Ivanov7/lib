package com.epam.bookreader.bisnesslayer.logic;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.bisnesslayer.session.SessionUser;
import com.epam.bookreader.bisnesslayer.session.Validation;
import com.epam.bookreader.bisnesslayer.session.command.Command;
import com.epam.bookreader.dao.AdminDao;
import com.epam.bookreader.dao.LogDao;
import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.SQLException;

public class UserLogic extends Option {
    private Console console;


    public UserLogic(Console console) {
        this.console = console;
    }

    public void go(String[] commands) {
        if (!SessionUser.isAdmin()) {
            console.printToConsole("Access denied!");
            return;
        }

        if (2 <= commands.length) {
            switch (commands[1]) {
                case "create":
                    create();
                    break;
                case "ban":
                    ban();
                    break;
                case "log":
                    getLogs();
                    break;
                case "get":
                    getAll();
                    break;
                default:
                    Command.messageNoRightCommand();
            }
        }
    }

    private void getLogs() {
        String login = console.inputValueInParam("Enter login for show logs: ");
        try {
            if (!new UserDao().isExistUser(login)) {
                console.printToConsole("User not exist");
                return;
            }
            int countRecord = console.enterNumber("How many last records show?");
            super.printListEntity(new LogDao().getCountLogs(login, countRecord), console);
        } catch (SQLException e) {
            console.printToConsoleError(e);
        }
    }

    private void getAll() {
        super.printListEntity(new AdminDao().getAll(), console);
    }

    private void ban() {
        String login = console.inputValueInParam("Enter login for baning : ");
        try {
            new AdminDao().delete(login);
        } catch (SQLException e) {
            console.printToConsoleError(e);
        }
    }

    private void create() {
        User user;
        String login = console.inputValueInParam("Enter login : ");
        String password = console.inputValueInParam("Enter password : ");
        if (Validation.isValidLogin(login) && Validation.isValidPassword(password))
            user = new User(login, DigestUtils.md5Hex(password));
        else {
            console.printToConsole("Not valid login or password");
            return;
        }
        try {
            new AdminDao().save(user);
            new LogDao().save(LogLogic.createLog("User create",
                    user.getUserUUID()));
        } catch (SQLException e) {
            console.printToConsoleError(e);
        }
    }
}
