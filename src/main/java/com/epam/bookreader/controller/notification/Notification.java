package com.epam.bookreader.controller.notification;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Notification {
    public static void addNotify(HttpServletRequest req, String message, Type type){
        HttpSession session = req.getSession();
        session.setAttribute("type", type);
        session.setAttribute("message", message);
    }
    public enum Type{
        SUCCESS,
        INFO,
        WARNING,
        DANGER
    }
}
