package com.epam.bookreader.bisnesslayer.logic.finder;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.bisnesslayer.session.command.Command;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.entity.Book;

import java.sql.SQLException;

public class BookFinder {
    private Console console;

    public BookFinder(Console console) {
        this.console = console;
    }

    public void findOnFiler(String[] command) throws SQLException {
        if (3 <= command.length)
            switch (command[2]) {
                case "title":
                    findOnTitle();
                    break;
                case "name":
                    findOnName();
                    break;
                case "ISBN":
                    findOnISBN();
                    break;
                case "year":
                    findOnYear();
                    break;
                case "years":
                    findOnYears();
                    break;
                default:
                    Command.messageNoRightCommand();
            }
    }

    private void findOnTitle() throws SQLException {
        String title = console.inputValueInParam("Enter title book for search: ");
        if (new BookDao().getBooksOnPartTitle(title).isEmpty()) console.printToConsole(
                "There are no data in the table with this parameters");
    }

    private void findOnName() throws SQLException {
        String name = console.inputValueInParam("Enter name Author or part name for search: ");
        Book book = new Book();
        //book.setAuthor(new Author(name));
        if (new BookDao().getBooksOnPartNameAuthor(book).isEmpty()) console.printToConsole(
                "There are no data in the table with this parameters");
    }

    private void findOnISBN() throws SQLException {
        String isbn = console.inputValueInParam("Enter isbn book for search: ");
        Book book = new Book();
        book.setIsbn(isbn);
        if (new BookDao().getBookOnISBN(book.getIsbn()) != null) console.printToConsole(
                "There are no data in the table with this parameters");
    }

    private void findOnYear() throws SQLException {
        int year = Integer.parseInt(console.inputValueInParam("Enter year book for search: "));
        Book book = new Book();

        book.setReleaseYear(year);
        if (new BookDao().getBookOnYear(book.getReleaseYear()).isEmpty()) console.printToConsole(
                "There are no data in the table with this parameters");
    }

    private void findOnYears() throws SQLException {
        int starYear = Integer.parseInt(console.inputValueInParam("Enter start year for search: "));
        int endYear = Integer.parseInt(console.inputValueInParam("Enter end year for search: "));
        if (new BookDao().getBooksOnYears(starYear, endYear).isEmpty()) console.printToConsole(
                "There are no data in the table with this parameters");
    }
}
