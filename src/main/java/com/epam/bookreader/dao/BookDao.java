package com.epam.bookreader.dao;

import com.epam.bookreader.connection.ConnectionDB;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao implements Dao<Book> {
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public BookDao(Connection con) {
        this.con = con;
    }

    public BookDao() {
        this.con = new ConnectionDB().getConnection("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@localhost:1521:xe",
                "SOUTHWIND", "1");
    }

    private Book fillForm(ResultSet rs) throws SQLException {
        return new Book(
                rs.getString("BookID"),
                rs.getString("ISBN"),
                rs.getString("title"),
                rs.getString("publisher"),
                rs.getInt("year"),
                rs.getInt("countPage"),
                rs.getBoolean("active"),
                new AuthorDao(con).get(rs.getString("authorId")));
    }

    public boolean isExistBook(Book book) throws SQLException {
        ps = con.prepareStatement("SELECT * FROM Books WHERE isbn = ?");
        ps.setString(1, book.getIsbn());
        return 0 < ps.executeUpdate();
    }

    @Override
    public Book get(String uuid) {
        return null;
    }

    public Book getBookOnISBN(String isbn) throws SQLException {
        List<Book> books = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM Books WHERE isbn = ?");
        ps.setString(1, isbn);
        rs = ps.executeQuery();
        while (rs.next()) books.add(fillForm(rs));
        return books.isEmpty() ? null: books.get(0);
    }

    @Override
    public List<Book> getAll() throws SQLException {
        List<Book> books = new ArrayList<>();
        Statement st = con.createStatement();
        rs = st.executeQuery("SELECT * FROM Books");
        while (rs.next()) books.add(fillForm(rs));
        return books;
    }

    public List<Book> getBooksOnPartNameAuthor(Book book) throws SQLException {
        List<Book> books = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM Books WHERE title LIKE ?");
        ps.setString(1, book.getAuthor() + "%");
        rs = ps.executeQuery();
        while(rs.next()) books.add(fillForm(rs));
        return books;
    }


    public List<Book> getBooksOnPartTitle(String partTitle) throws SQLException {
        List<Book> books = new ArrayList<>();
        ps = con.prepareStatement("SELECT * FROM Books WHERE title LIKE ?");
        ps.setString(1, partTitle + "%");
        rs = ps.executeQuery();
        while (rs.next()) books.add(fillForm(rs));
        return books;
    }

    @Override
    public boolean save(Book book) throws SQLException {
        ps = con.prepareStatement("INSERT INTO Books VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)");
        ps.setString(1, book.getBookUUID());
        ps.setString(2, book.getIsbn());
        ps.setString(3, book.getBookName());
        ps.setInt(4, book.getReleaseYear());
        ps.setInt(5, book.getPageCount());
        ps.setString(6, book.getPublisher());
        ps.setInt(7, book.isActive() ? 1 : 0);
        ps.setString(8, book.getAuthor().getAuthorUUID());
        return ps.execute();
    }

    @Override
    public boolean delete(Book book) throws SQLException {
        ps = con.prepareStatement("UPDATE books SET active = 0 WHERE isbn = ?");
        ps.setString(1, book.getIsbn());
        return ps.execute();
    }

    public boolean delete(String uuid) throws SQLException {
        ps = con.prepareStatement("UPDATE books SET active = 0 WHERE bookId = ?");
        ps.setString(1, uuid);
        return ps.execute();
    }


    public boolean delete(Author author) throws SQLException {
        ps = con.prepareStatement("UPDATE Books SET active = 0 WHERE authorId = ?");
        ps.setString(1, author.getAuthorUUID());
        return ps.execute();
    }

    public List<Book> getBooksOnYears(int starYear, int endYear) throws SQLException {
        List<Book> books = new ArrayList<>();
        ps = con.prepareStatement("SELECT bookId, isbn, title, year, numberOfPages, b.active AS active_b, " +
                "a.authorId, fullName, a.active AS active_a " +
                "FROM Books b LEFT JOIN Authors a ON b.authorId = a.authorId " +
                "WHERE year >= ? AND year <= ? AND a.fullName IS NOT NULL");
        ps.setInt(1, starYear);
        ps.setInt(2, endYear);
        rs = ps.executeQuery();
        while (rs.next()) books.add(fillForm(rs));
        return books;
    }

    public List<Book> getBookOnYear(int year) throws SQLException {
        List<Book> books = new ArrayList<>();
        ps = con.prepareStatement("SELECT bookId, isbn, title, year, numberOfPages, b.active AS active_b, " +
                "a.authorId, fullName, a.active AS active_a " +
                "FROM Books b LEFT JOIN Authors a ON b.authorId = a.authorId " +
                " WHERE year = ? AND a.fullName IS NOT NULL )");
        ps.setInt(1, year);
        rs = ps.executeQuery();
        while (rs.next()) books.add(fillForm(rs));
        return books;
    }
}
