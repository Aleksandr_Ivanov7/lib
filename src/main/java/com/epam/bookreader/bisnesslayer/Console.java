package com.epam.bookreader.bisnesslayer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class Console {
    private static final Logger log = LogManager.getLogger(Console.class);
    private static Scanner scanner = new Scanner(System.in);

    private boolean validate(String command) throws SQLException {
        if (command.equalsIgnoreCase("exit")) {
            printToConsole("Exit from application");
            return false;
        }
        if (command.equals("reg")) new Registration(this);
        if (command.equals("sing in")) new LogIn(this);
        else log.info("The command is not recognized. Enter command again.\n");
        return true;
    }

    public String inputValue(String string) {
        return string == null ? scanner.nextLine() : string;
    }

    public void printToConsole(String string) {
        log.info(string + "\n");
    }

    public void printToConsoleError(Exception e) {
        log.error(e);
    }

    public String inputValueInParam(String msg) {
        printToConsole(msg);
        return inputValue(null);
    }

    public int enterNumber(String userEnter){
        int number = 0;
        try {
            number = Integer.parseInt(inputValueInParam(userEnter));
        } catch (NumberFormatException e){
            printToConsoleError(e);
            printToConsole("\n Enter number.");
            enterNumber(userEnter);
        }
        return number;
    }

    public void startApp() throws SQLException {
        do {
            log.info("\n[sing in] command for enter\n");
            log.info("[reg] command for registration\n");
            log.info("[exit] command for exit from app\n");
        } while (validate(inputValue(null)));
        scanner.close();
    }
}
