package com.epam.bookreader.bisnesslayer.logic;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.bisnesslayer.session.SessionUser;
import com.epam.bookreader.bisnesslayer.session.command.Command;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.dao.BookmarkDao;
import com.epam.bookreader.dao.LogDao;
import com.epam.bookreader.entity.Book;
import com.epam.bookreader.entity.Bookmark;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class BookmarkLogic extends Option {
    private Console console;

    public BookmarkLogic(Console console) {
        this.console = console;
    }

    public void go(String[] commands) throws SQLException {
        if (2 <= commands.length)
            switch (commands[1]) {
                case "add":
                    preAdd();
                    break;
                case "rm":
                    preRemove();
                    break;
                case "get":
                    get();
                    break;
                case "find":
                    System.out.println("В процессе разработки");
                    break;
                default:
                    Command.messageNoRightCommand();
            }
    }

    private Bookmark createBookmark(Book book) {
        return new Bookmark(
                UUID.randomUUID().toString(),
                SessionUser.user,
                book,
                console.enterNumber("Enter number page for save: "),
                true);
    }

    private void preRemove() throws SQLException {
        console.printToConsole("Remove bookmark... ");
        console.printToConsole("List of BookMark:");
        super.printListEntity(new BookmarkDao().getAll(SessionUser.user), console);
        Bookmark bookMark = (Bookmark) super.getEntity();
        remove(bookMark);
    }

    private void remove(Bookmark bookMark) throws SQLException {
        new BookmarkDao().delete(bookMark);
        console.printToConsole("Bookmark deleted!");
        new LogDao().save(new LogLogic().createLog("Bookmark delete",
                bookMark.getUuid()));
    }

    private void get() throws SQLException {
        super.printListEntity(new BookmarkDao().getAll(SessionUser.user), console);
    }

    private void preAdd() throws SQLException {
        super.printListEntity(searchBooks(console.inputValueInParam("Enter name or part name book: ")), console);
        Book book = (Book) super.getEntity();
        Bookmark bookMark = createBookmark(book);
        add(bookMark);
    }

    private void add(Bookmark bookMark) throws SQLException {
        new BookmarkDao().save(bookMark);
        console.printToConsole("Bookmark add!");
        new LogDao().save(LogLogic.createLog("Bookmark create",
                bookMark.getUuid()));
    }

    private List<Book> searchBooks(String partName) throws SQLException {
        return new BookDao().getBooksOnPartTitle(partName);
    }
}
