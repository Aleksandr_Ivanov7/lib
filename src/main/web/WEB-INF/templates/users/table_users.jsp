<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
    <table id="table-users" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Login</th>
            <th scope="col">Admin</th>
            <th scope="col">Active</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <% int i = 0;%>
        <c:forEach var="user" items="${users}">
            <tr>
                <th scope="row"><%=++i%>
                </th>
                <td><c:out value="${user.login}"/></td>
                <td><c:out value="${user.role.name}"/></td>
                <td><c:out value="${user.active}"/></td>
                <td>
                    <a href="${pageContext.request.contextPath}/user/edit?uuid=${user.userUUID}">edit</a>
                    <a href="${pageContext.request.contextPath}/user/delete?login=${user.login}">remove</a>
                    <a href="${pageContext.request.contextPath}/user/logs/show?login=${user.login}">show(logs)</a>
                </td>
                <td></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>