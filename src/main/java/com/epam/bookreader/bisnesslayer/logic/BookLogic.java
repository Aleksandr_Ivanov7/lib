package com.epam.bookreader.bisnesslayer.logic;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.bisnesslayer.logic.finder.BookFinder;
import com.epam.bookreader.bisnesslayer.session.command.Command;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.dao.LogDao;
import com.epam.bookreader.entity.Book;

import java.sql.SQLException;

public class BookLogic extends Option {
    private Console console;

    public BookLogic(Console console) {
        this.console = console;
    }

    public void go(String[] command) throws SQLException {
        if (2 <= command.length)
            switch (command[1]) {
                case "add":
                    add();
                    break;
                case "rm":
                    preRemove();
                    break;
                case "get":
                    getAll();
                    break;
                case "find":
                    new BookFinder(console).findOnFiler(command);
                    break;
                default:
                    Command.messageNoRightCommand();
            }
    }

    private Book createBook() {
        return new Book(
                console.inputValueInParam("Enter ISBN: "),
                console.inputValueInParam("Enter Title: "),
                console.inputValueInParam("Enter Publisher"),
                console.enterNumber("Enter Year: "),
                console.enterNumber("Enter Count of page: "),
                true,
                new AuthorLogic(console).add());
    }

    public Book add() {
        console.printToConsole("Adding book...");
        try {
            Book book = createBook();
            BookDao bookDao = new BookDao();
            if (!bookDao.isExistBook(book)) {
                bookDao.save(book);
                new LogLogic();
                new LogDao().save(LogLogic.createLog("Book create", book.getBookUUID()));
                console.printToConsole("Book create!");
            }
            return book;
        } catch (SQLException e) {
            console.printToConsoleError(e);
            return null;
        }
    }

    private void preRemove() throws SQLException {
        console.printToConsole("Enter <ISBN> book, what you want to delete");
        String isbn = console.inputValueInParam("Enter ISBN : ");
        Book book = new BookDao().getBookOnISBN(isbn);
        if (book != null) remove(book);
        else console.printToConsole("Book not found");
    }

    public void getAll() throws SQLException {
        super.printListEntity(new BookDao().getAll(), console);
    }

    private void remove(Book book) throws SQLException {
        new BookDao().delete(book);
        console.printToConsole("Deleted!");
        new LogDao().save(LogLogic.createLog("Delete book",
                book.getBookUUID()));
    }

    public void get(String isbn) throws SQLException {
        new BookDao().getBookOnISBN(isbn);
    }
}
