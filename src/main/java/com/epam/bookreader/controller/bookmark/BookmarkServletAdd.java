package com.epam.bookreader.controller.bookmark;


import com.epam.bookreader.controller.log.Logger;
import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.dao.BookmarkDao;
import com.epam.bookreader.entity.Bookmark;
import com.epam.bookreader.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/bookmarks/add")
public class BookmarkServletAdd extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/bookmarks/bookmark_add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getServletContext().getAttribute("user");
        try {
            Bookmark bookmark = new Bookmark(
                    user,
                    new BookDao().getBookOnISBN(req.getParameter("isbn").trim()),
                    Integer.parseInt(req.getParameter("number-page")),
                    true);
            new BookmarkDao().save(bookmark);
            Logger.createLog(user.getLogin(), "Bookmark add", bookmark.getUuid());
            Notification.addNotify(req, "Bookmark successfully added!", Notification.Type.SUCCESS);
        } catch (SQLException e) {
            Notification.addNotify(req, "Error! Bookmark and author don't added! " + e, Notification.Type.DANGER);
        } finally {
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }
}
