package com.epam.bookreader.controller.filter;

import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.RoleDao;
import com.epam.bookreader.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(urlPatterns = "/user/*")
public class AdminFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        User user = (User) req.getServletContext().getAttribute("user");
        if ((user.getRole().getName().equalsIgnoreCase("admin") ||
                user.getRole().getName().equalsIgnoreCase("moderator"))
                && user.isActive()) {
            try {
                session.setAttribute("roles", new RoleDao().getAll());
                chain.doFilter(request, response);
            } catch (SQLException e) {
                Notification.addNotify(req, "Error! Roles don't get" + e, Notification.Type.DANGER);
            }
        } else {
            Notification.addNotify(req, "Access denied! ", Notification.Type.WARNING);
            resp.sendRedirect(req.getContextPath() + "/library");
        }
    }

    @Override
    public void destroy() {
    }
}
