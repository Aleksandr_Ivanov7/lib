<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${user.role.name != 'moderator'}">
    <li class="nav-item">
        <a class="nav-link active" id="pills-books-tab" data-toggle="pill" href="#pills-books"
           role="tab" aria-controls="pills-books" aria-selected="true">Books</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pills-authors-tab" data-toggle="pill" href="#pills-authors" role="tab"
           aria-controls="pills-authors" aria-selected="false">Authors</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pills-bookmarks-tab" data-toggle="pill" href="#pills-bookmarks"
           role="tab" aria-controls="pills-bookmarks" aria-selected="false">Bookmarks</a>
    </li>
</c:if>
