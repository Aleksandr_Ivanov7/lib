package com.epam.bookreader.bisnesslayer.logic;

import com.epam.bookreader.bisnesslayer.Console;
import com.epam.bookreader.entity.Entity;

import java.util.Iterator;
import java.util.List;

public abstract class Option<T extends Entity> {
    private List<T> entityList;
    private Console console;


    final void printListEntity(List<T> list, Console console) {
        entityList = list;
        this.console = console;
        int i = 0;
        if (entityList.isEmpty()) {
            console.printToConsole("List is Empty.");
            return;
        }
        Iterator<T> iter = entityList.iterator();
        while(iter.hasNext()) {
            T entity = iter.next();
            if(entity.isActive()) {
                console.printToConsole(i + " : " + entity.toString());
                i++;
            }else iter.remove();
        }
    }

    final public T getEntity() {
        if (entityList.isEmpty()){
            console.printToConsole("Books not found");
            return null;
        }
        int choice = 0;
        try {
            choice = Integer.parseInt(console.inputValueInParam("Enter index : "));
        }catch (NumberFormatException e){
            console.printToConsoleError(e);
            console.printToConsole("\nEnter index again yet");
            getEntity();
        }
        return entityList.get(choice);
    }
}
