package com.epam.bookreader.entity;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Book implements Entity {
    private String bookUUID;
    @JsonProperty("ISBN")
    private String isbn;
    @JsonProperty("bookName")
    private String bookName;
    @JsonProperty("publisher")
    private String publisher;
    @JsonProperty("releaseYear")
    private int releaseYear;
    @JsonProperty("pageCount")
    private int pageCount;
    private boolean active;
    @JsonIdentityReference
    private Author author;

    public Book(String bookUUID, String isbn, String bookName, String publisher, int releaseYear, int pageCount, boolean active, Author author) {
        this.bookUUID = bookUUID;
        this.isbn = isbn;
        this.bookName = bookName;
        this.publisher = publisher;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.active = active;
        this.author = author;
    }

    public Book(String isbn, String bookName, String publisher, int releaseYear, int pageCount, boolean active, Author author) {
        this.publisher = publisher;
        this.bookUUID = setUUID();
        this.isbn = isbn;
        this.bookName = bookName;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.active = active;
        this.author = author;
    }

    public void setBookUUID(String bookUUID) {
        this.bookUUID = bookUUID;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Book() {
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getBookUUID() {
        return bookUUID;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getBookName() {
        return bookName;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public int getPageCount() {
        return pageCount;
    }

    public boolean isActive() {
        return active;
    }

    public Author getAuthor() {
        return author == null ? new Author() : author;
    }

}
