package com.epam.bookreader.controller.user;

import com.epam.bookreader.controller.notification.Notification;
import com.epam.bookreader.dao.LogDao;
import com.epam.bookreader.entity.Log;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/user/logs/show")
public class UserServletLogShow extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Log> logs = new ArrayList<>();
        String login = req.getParameter("login");
        try {
            logs = new LogDao().getAll(login);
            if(logs.isEmpty()) Notification.addNotify(req, "User don't do something ", Notification.Type.INFO);
        } catch (SQLException e) {
            Notification.addNotify(req, "Error loading logs" + e, Notification.Type.DANGER);
        }
        req.setAttribute("logs", logs);
        req.getRequestDispatcher("/WEB-INF/templates/admin/table_logs.jsp").forward(req, resp);
    }
}
