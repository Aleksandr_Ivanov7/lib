package com.epam.bookreader.controller.author;

import com.epam.bookreader.entity.Author;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;


@WebServlet(urlPatterns = "/authors")
public class AuthorController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/authors/table_authors.jsp").forward(req, resp);
        req.setAttribute("author", new Author("name Author",
                "Sec name author",
                "last name author",
                Date.valueOf(String.valueOf(System.currentTimeMillis()))));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(req.getContextPath() + "/library");
    }
}
